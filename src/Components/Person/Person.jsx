import PropTypes from 'prop-types';
import style from "./Person.module.css";


const Person = function(props)
{ 
    const min = 1;
    const max = 100;
    const rand = min + Math.random() * (max - min);
    let name =props.Nom;
    let age = props.Age===undefined?Math.floor(rand):props.Age; 
    const BoxClass = age<18?style.PersonBox+' '+style.PersonBoxRed : style.PersonBox;
     
    
    return(      
        <div className={BoxClass}>
            <p>Je suis {name} et
                j'ai {age} ans !
            </p>
        </div>
    )
};

Person.propTypes=
{
    Nom:PropTypes.string,
    Age:PropTypes.number
};

Person.defaultProps=
{
    Nom:"Hugo"
}

export default Person;