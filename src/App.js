import './App.css';
import Person from './Components/Person/Person';

function App() {
  return (
    <div className="App">
       <Person/> 
       <Person Nom='Mike'/>
       <Person Nom='Léon' Age={42}/> 
       <Person Age={5}/>
       <Person Nom="Julie" Age={15}/>
       <Person/> 
       <Person/> 
       <Person/>  
    </div>
  );
}

export default App;
